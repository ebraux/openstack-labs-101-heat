---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)

# Openstack

## Utilisation de heat

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Utilisation de heat

##  Utilisation avec Horizon

---

# Licence informations

Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Utilisation de heat

##  Utilisation avec Horizon
---

# Chargement de la template

- Dans le menu de navigation sur le côté gauche, ouvrir le menu `Projet` / `Orchestration` et choisir  la catégory `Stacks`
- Cliquer sur le bouton `Lancer la Stack` (en haut à droite)
- Renseigner le modèle (fichier template HOT ), 
**depuis une URL, sans utiliser d'environnement** : [os-admin.yaml](os-admin.yaml)

---
# Saisie des parametres 

- Nom de la stack : Un nom "parlant", **os-admin** par exemple
- Délai d'attente à la création: laisser la *valeur par défaut : 60mn*
- Mot de passe de l'utilisateur : 
  - cette option peut être ignorée si on pas besoin des fonctionnalités HA et autoscaling. 
  - Mais dans Horizon, c'est un champ obligatoire.
  - Il faut saisir quelque chose, mais cette information n'est pas utilisée ensuite.
- keypair
- ...
 
---

# Lancement de la stack

- Cliquer ensuite sur **"Lancer la stack"**. Votre stack apparait dans la liste des stacks.
- Cliquer sur votre stack, puis sur l'onglet **"Topologie"** pour suivre en direct la création des différents éléments.
- Cliquer sur l'onglet **"Vue d'enssemble"** pour consulter :
  - les informations sur la stack et son état.
  - les différentes informations de déploiement (projet, keypair, ...).
  - les `outputs` et dans le cas présent l'**adresse IP flottante** de l'instance.

---

# Vérifier le déploiement

Consulter :

- la liste des routeurs
- la liste des réseaux
- la liste des instances
- ...
 
---

## Ménage

- Cliquer ensuite sur **"Supprimer la stack"**. 
- Toutes les ressources sont détruites
