# Lab de prise en main d'Openstack avec l'outil d'orchestration Heat

La documentation générée est disponible sur le site [https://ebraux.gitlab.io/openstack-labs-101-heat/](https://ebraux.gitlab.io/openstack-labs-101-heat/)


## Test en local des slides

Génération du PDF
```bash
cd docs/slides
mkdir output
chmod 777 output
docker run -it --rm  --init -v ${PWD}:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli openstack-labs-101-heat.md --allow-local-files --pdf -o output/openstack-labs-101-heat.pdf
```

Watch mode
```bash
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -p 37717:37717 marpteam/marp-cli -w openstack-labs-101-heat.md
```

Server mode (Serve current directory in http://localhost:8080/)
```bash
docker run --rm --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 37717:37717 marpteam/marp-cli -s .
```


## Tests en local du site

Build de l'image
```bash
docker build -t mkdocs_openstack-labs-101-heat .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs-101-heat mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs-101-heat mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_openstack-labs-101-heat rm -rf /work/site
docker image rm mkdocs_intro-cloud
```
Rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_openstack-labs-101-heat mkdocs new openstack-labs-101-heat
sudo chown -R $(id -u -n):$(id -g -n)  *


